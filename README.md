
# leaflet-tilelayer-terrainel

[Leaflet](https://leafletjs.com/) Terrain Elevation Plugin

# NPM

This package is available on [npm](https://www.npmjs.com/):

[@lowswaplab/leaflet-tilelayer-terrainel](https://www.npmjs.com/package/@lowswaplab/leaflet-tilelayer-terrainel)

This package can be installed by performing:

    npm install @lowswaplab/leaflet-tilelayer-terrainel

# JavaScript Example:

    import "@lowswaplab/leaflet-tilelayer-terrainel";

    L.tileLayer.terrainEl("https://api.mapbox.com/v4/mapbox.terrain-rgb/{z}/{x}/{y}.pngraw?access_token=" + mapboxAccessToken,
      {
      maxNativeZoom: 14,
      maxZoom: 23,
      opacity: 0
      }).addTo(map);

# Source Code

[leaflet-tilelayer-terrainel](https://gitlab.com/lowswaplab/leaflet-tilelayer-terrainel)

# Contribution

If you find this software useful, please consider financial support
for future development via
[PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GY9C5FN54TCKL&source=url).

# Author

[Low SWaP Lab](https://www.lowswaplab.com/)

# Copyright

Copyright © 2020 Low SWaP Lab [lowswaplab.com](https://www.lowswaplab.com/)

