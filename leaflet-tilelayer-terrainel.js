
// Leaflet Terrain Elevation plugin

// https://gitlab.com/lowswaplab/leaflet-tilelayer-terrainel

// Copyright © 2020 Low SWaP Lab lowswaplab.com

"use strict";

(function()
  {
  L.TileLayer.TerrainEl = L.TileLayer.extend(
    {
    options:
      {
      crossOrigin: "anonymous"
      },

/*
    // XXX this causes problems
    initialize: function(options)
      {
      L.setOptions(this, options);
      },
*/

    // returns terrain elevation (in meters) or NaN
    getElevation: function(latlng)
      {
      var rgb=this._getColor(latlng);
      var elev=NaN;

      if (rgb !== null)
        {
        elev = -10000 + ((rgb[0]*256*256 + rgb[1]*256 + rgb[2])*0.1);
        elev = Math.round(elev);
        };

      return(elev);
      },

    /*

      The _getColor() function is from leaflet-tilelayer-colorpicker:

      https://github.com/frogcat/leaflet-tilelayer-colorpicker

      MIT License

      Copyright (c) 2017 Yuzo Matsuzawa

      Permission is hereby granted, free of charge, to any person
      obtaining a copy of this software and associated documentation
      files (the "Software"), to deal in the Software without restriction,
      including without limitation the rights to use, copy, modify, merge,
      publish, distribute, sublicense, and/or sell copies of the Software,
      and to permit persons to whom the Software is furnished to do so,
      subject to the following conditions:

      The above copyright notice and this permission notice shall be
      included in all copies or substantial portions of the Software.

      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
      BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
      ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
      CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
      SOFTWARE.
    */
    _getColor: function(latlng)
      {
      var size = this.getTileSize();
      var point = this._map.project(latlng, this._tileZoom).floor();
      var coords = point.unscaleBy(size).floor();
      var offset = point.subtract(coords.scaleBy(size));
      coords.z = this._tileZoom;
      var tile = this._tiles[this._tileCoordsToKey(coords)];
      if (!tile || !tile.loaded) return null;
      try {
        var canvas = document.createElement("canvas");
        canvas.width = 1;
        canvas.height = 1;
        var context = canvas.getContext('2d');
        context.drawImage(tile.el, -offset.x, -offset.y, size.x, size.y);
        return context.getImageData(0, 0, 1, 1).data;
        }
      catch (e)
        {
        return null;
        }
      }

    });

  L.tileLayer.terrainEl = function(url, options)
    {
    return(new L.TileLayer.TerrainEl(url, options));
    };
  }
)();

